package locker.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.BlockingQueue;

public class LICReadThread extends Thread {
  private static Logger logga = LogManager.getLogger();
  PriorityQueue<Integer> myPriorityQueueLock;
  LockerImplementationClass lic = new LockerImplementationClass();
  public LICReadThread (PriorityQueue<Integer> myPriorityQueueLock) {
    this.myPriorityQueueLock = myPriorityQueueLock;
  }
  @Override
  public void run() {
    try {
      lic.lockWriteAccess();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      putElemToArrayList(myPriorityQueueLock);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      lic.unlockWriteAccess();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  private void putElemToArrayList(PriorityQueue<Integer> myPriorityQueueLock)
    throws InterruptedException {
    for (int i = 0; i < 20; i++) {
      logga.info("poll= " + i);
      myPriorityQueueLock.poll();
      Thread.sleep(20);
    }
  }
}
