package locker.model;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class ThreadClass extends Thread {
  LockerImplementationClass lic = new LockerImplementationClass();
  @Override
  public void run() {
    try {
      lic.lockWriteAccess();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      int[] arrayToSort = new int[30];
      for (int i = 0; i < arrayToSort.length; i++) {
        arrayToSort[i] = ThreadLocalRandom.current().nextInt(0, 100);
      }
      Arrays.sort(arrayToSort);
      for (int element : arrayToSort) {
        System.out.print(element + " ");
      }
      System.out.println();
    } finally {
      try {
        lic.unlockWriteAccess();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
