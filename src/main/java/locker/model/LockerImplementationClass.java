package locker.model;

import java.util.HashMap;
import java.util.Map;

public class LockerImplementationClass {
  private Map<Thread, Integer> readingThreads = new HashMap<>();
  private Map<Thread, Integer> writingThreads = new HashMap<>();
  private int requestsWrite = 0;
  private int accessWrite = 0;
  private Thread threadsWriting = null;
  public synchronized void lockWriteAccess() throws InterruptedException {
    requestsWrite++;
    Thread threadCalled = Thread.currentThread();
    while (!canGiveWriteAccess(threadCalled)) {
      wait();
    }
    requestsWrite--;
    accessWrite++;
    threadsWriting = threadCalled;
  }
  public synchronized void unlockWriteAccess() throws InterruptedException {
    accessWrite--;
    if (accessWrite == 0) {
      threadsWriting = null;
    }
    notifyAll();
  }
  private boolean canGiveWriteAccess(Thread threadCalled) {
    if (hasActiveReader()) {
      return false;
    }
    if (threadsWriting == null) {
      return true;
    }
    if (!isWritingThread(threadCalled)) {
      return false;
    }
    return true;
  }
  private boolean hasActiveReader() {
    return writingThreads.size() > 0;
  }
  private boolean isWritingThread(Thread threadCalled) {
    return threadsWriting == threadCalled;
  }


  public synchronized void lockReadAccess() throws InterruptedException{
    Thread callingThread = Thread.currentThread();
    while (!canGiveReadAccess(callingThread)) {
      wait();
    }
    readingThreads.put(callingThread,
      (readAccessCount(callingThread) + 1));
  }
  public synchronized void unlockReadAccess(){
    Thread callingThread = Thread.currentThread();
    int accessCount = readAccessCount(callingThread);
    if(accessCount == 1){ readingThreads.remove(callingThread); }
    else { readingThreads.put(callingThread, (accessCount -1)); }
    notifyAll();
  }
  private boolean canGiveReadAccess(Thread callingThread){
    if(accessWrite > 0) {
      return false;
    }
    if(isReadingThread(callingThread)) {
      return true;
    }
    if(requestsWrite > 0) {
      return false;
    }
    return true;
  }
  private int readAccessCount(Thread callingThread){
    Integer accessCount = readingThreads.get(callingThread);
    if(accessCount == null) {
      return 0;
    }
    return accessCount.intValue();
  }
  private boolean isReadingThread(Thread callingThread){
    return readingThreads.get(callingThread) != null;
  }
}

