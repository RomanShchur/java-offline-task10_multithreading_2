package locker.controller;

import locker.model.LICReadThread;
import locker.model.LICWriteThread;
import locker.model.LockerImplementationClass;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class LockerImplementationMainClass {
  public static void main(String[] args) {
    LockerImplementationClass lic = new LockerImplementationClass();
    PriorityQueue<Integer> myPriorityQueueLock = new PriorityQueue<>();
    new Thread(new LICWriteThread(myPriorityQueueLock)).start();
    new Thread(new LICWriteThread(myPriorityQueueLock)).start();
    new Thread(new LICWriteThread(myPriorityQueueLock)).start();
    new Thread(new LICWriteThread(myPriorityQueueLock)).start();

    new Thread(new LICReadThread(myPriorityQueueLock)).start();
    new Thread(new LICReadThread(myPriorityQueueLock)).start();
  }
}
