package synchonizedThreads.controller;

import synchonizedThreads.model.SynchronizedThreadsClass;
import synchonizedThreads.model.UnSyncronyzedThreads;

public class SynchronizedThreadsMain {
  public static void main(String[] args) {
    int ThreadCount = 5;
    SynchronizedThreadsClass[] threads = new SynchronizedThreadsClass[ThreadCount];
    for (int i = 0; i < threads.length; i++) {
      threads[i] = new SynchronizedThreadsClass();
      threads[i].start();
    }

    UnSyncronyzedThreads[] threadsUnsync =
      new UnSyncronyzedThreads[ThreadCount];
    for (int i = 0; i < threads.length; i++) {
      threadsUnsync[i] = new UnSyncronyzedThreads();
      threadsUnsync[i].start();
    }
  }
}
