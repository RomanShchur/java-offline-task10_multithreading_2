package synchonizedThreads.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizedThreadsClass extends Thread {
  private static Logger logga = LogManager.getLogger();
  private Lock someLock = new ReentrantLock();
  @Override
  public void run() {
    someLock.lock();
    try {
      int[] arrayToSort = new int[30];
      for (int i = 0; i < arrayToSort.length; i++) {
        arrayToSort[i] = ThreadLocalRandom.current().nextInt(0, 100);
      }
      Arrays.sort(arrayToSort);
      for (int element : arrayToSort) {
        System.out.print(element + " ");
      }
      System.out.println();
    } finally {
      someLock.unlock();
    }
  }
}

