package blockingQueue.controller;

import blockingQueue.model.BQThreadGetterClass;
import blockingQueue.model.BQThreadPutterClass;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class BlokingQueueMainClass {


  public static void main(String[] args) {
    BlockingQueue<Integer> blocQueue = new LinkedBlockingDeque<>(80);
    new Thread(new BQThreadPutterClass(blocQueue)).start();
    new Thread(new BQThreadPutterClass(blocQueue)).start();
    new Thread(new BQThreadPutterClass(blocQueue)).start();
    new Thread(new BQThreadPutterClass(blocQueue)).start();

    new Thread(new BQThreadGetterClass(blocQueue)).start();
    new Thread(new BQThreadGetterClass(blocQueue)).start();
  }
}
