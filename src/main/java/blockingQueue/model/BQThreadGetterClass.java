package blockingQueue.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;

public class BQThreadGetterClass extends Thread {
  private static Logger logga = LogManager.getLogger();
  private final BlockingQueue<Integer> blocQueue;
  public BQThreadGetterClass(BlockingQueue<Integer> blocQueue) {
    this.blocQueue = blocQueue;
  }
  @Override
  public void run() {
    try {
      while (true) {
        getterMethod(blocQueue.take());
      }
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }
  private void getterMethod(Integer take) throws InterruptedException {
    logga.info("Took= " + take);
    Thread.sleep(20);
    if (blocQueue.remainingCapacity() >= 80) {
      System.exit(0);
    }
  }
}
