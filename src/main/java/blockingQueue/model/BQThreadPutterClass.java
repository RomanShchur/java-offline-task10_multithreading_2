package blockingQueue.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

public class BQThreadPutterClass extends Thread {
  private static Logger logga = LogManager.getLogger();
  private final BlockingQueue<Integer> blocQueue;
  public BQThreadPutterClass(BlockingQueue<Integer> blocQueue) {
    this.blocQueue = blocQueue;
  }
  @Override
  public void run() {
    try {
      putElemToQueue();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
    }
  }
  private void putElemToQueue() throws InterruptedException  {
    for (int i = 0; i < 20; i++) {
      logga.info("Put= " + i);
      blocQueue.put(i);
      Thread.sleep(10);
    }
  }
}
